import matplotlib
matplotlib.use('Agg')
import torch
import torchvision
import object_detection_dataset as odd
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from metrics import non_maximum_suppression, average_precision
import faster_rcnn
import time
import copy
import os
import argparse
import uuid

# os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

def draw_detections(image, bbox, class_name, figure_path):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.imshow(image)
    for i in range(len(bbox)):
        if (class_name[i] != 'background'):
            patch = patches.Rectangle((bbox[i, 0], bbox[i, 1]), bbox[i, 2], bbox[i, 3], linewidth = 1, edgecolor = 'r', facecolor = 'none')
            ax.add_patch(patch)
            ax.annotate(class_name[i], (bbox[i, 0], bbox[i, 1])) 
    fig.savefig(figure_path)
    plt.close(fig)

def train_faster_rcnn(model, optimizer, scheduler, dataset, config):
    since = time.time()

    best_model_weights = copy.deepcopy(model.state_dict())
    best_map_mean = -1
    for epoch in range(config['num_epochs']):
        print('Epoch: {}/{}'.format(epoch, config['num_epochs'] - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()
            else:
                model.eval()

            running_loss = 0.0
            annotations, confidence_list, prediction_list, final_bbox_list = [], [], [], []
            dataset[phase].shuffle()
            # Iterate over data
            epoch_time = time.time()
            for i, samples in enumerate(dataset[phase]):
                inputs = samples['image'].to(device).unsqueeze(0)
                inputs = inputs.float()
                labels = samples['annotations'].to(device).unsqueeze(0)
                labels = labels.float()

                # Zero the parameter gradient
                optimizer.zero_grad()

                # Forward
                # Track history if only in train
                # step_time = time.time()
                with torch.set_grad_enabled(phase == 'train'):
                    final_bbox, final_cls, loss_rpn_cls, loss_rpn_tfm, loss_cls, loss_cls_tfm = model(inputs, actual_size = inputs.size(), labels = labels)
                    loss = (loss_rpn_cls + loss_rpn_tfm + loss_cls + loss_cls_tfm).mean()
                    # Backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # Statistics
                running_loss += loss.item() * inputs.size(0)
                # Save proposals to compute mAP later
                with torch.set_grad_enabled(False):
                    if (final_cls.numel() > 0):
                        soft = torch.nn.Softmax(dim = 1)
                        final_cls[:, 1:] = soft(final_cls[:, 1:])
                        confidence, prediction = torch.max(final_cls[:, 1:], dim = 1)
                    for k in range(inputs.size(0)):
                        if (final_cls.numel() == 0):
                            confidence_list.append(torch.tensor([], device = device))
                            prediction_list.append(torch.tensor([], device = device))
                            final_bbox_list.append(torch.tensor([], device = device))
                            annotations.append(labels[k, :, :].view(-1, 5).cpu().numpy())
                            break
                        bbox_k = final_bbox[final_bbox[:, 0] == k, 1:]
                        conf_k = confidence[final_bbox[:, 0] == k]
                        pred_k = prediction[final_bbox[:, 0] == k]
                        # Filter predictions, for every class, with NMS
                        filter_pred_list, filter_conf_list, filter_bbox_list = [], [], []
                        for j in range(len(config['class_list'])):
                            bbox = bbox_k[pred_k == j, :]
                            conf = conf_k[pred_k == j]
                            if (len(bbox) > 0):
                                conf, bbox = non_maximum_suppression(conf, bbox.transpose(0, 1), 0.3)
                                bbox = bbox.transpose(0, 1)
                                filter_conf_list.append(conf)
                                filter_pred_list.append(j * torch.ones(conf.size(), dtype = torch.long, device = device))
                                filter_bbox_list.append(bbox)
                        if (len(filter_conf_list) > 0):
                            conf_k = torch.cat(filter_conf_list)
                            pred_k = torch.cat(filter_pred_list)
                            bbox_k = torch.cat(filter_bbox_list)
                        else:
                            conf_k = torch.tensor([], device = device)
                            pred_k = torch.tensor([], device = device)
                            bbox_k = torch.tensor([], device = device)
                        confidence_list.append(conf_k.cpu().numpy())
                        prediction_list.append(pred_k.cpu().numpy())
                        final_bbox_list.append(bbox_k.cpu().numpy())
                        annotations.append(labels[k, :, :].view(-1, 5).cpu().numpy())
                # step_time = time.time() - step_time
                # print('Step time: {:.4f}s'.format(step_time))
            epoch_loss = running_loss / len(dataset[phase])
            epoch_time = time.time() - epoch_time
            print('Epoch complete in {:.0f}h {:.0f}m {:.0f}s'.format(epoch_time // 3600, epoch_time // 60 % 60, epoch_time % 60))
            print('{} Loss: {:.4f}'.format(phase, epoch_loss))
            map_time = time.time()
            ap = average_precision(confidence_list, prediction_list, final_bbox_list, annotations, dataset[phase].difficult_list, config['class_list'], config['mAP@'], device)
            map_mean = ap.mean()
            map_time = time.time() - map_time
            print('mAP computed in {:.0f}m {:.0f}s'.format(map_time // 60, map_time % 60))
            print('{} mAP: {:.4f}%'.format(phase, 100 * map_mean))
            
            # If this is the best model, deep copy it
            if phase == 'val' and map_mean > best_map_mean:
                best_map_mean = map_mean
                best_model_weights = copy.deepcopy(model.state_dict())

            if phase == 'val':
                torch.save({'model': model.state_dict(), 'optimizer': optimizer.state_dict(), 'scheduler': scheduler.state_dict(), 'epoch': epoch + 1, 'ap': ap, 'config': config}, './data/weights/faster_rcnn_{}_{}_{}_{}.pth'.format(config['base_model'], config['dataset'], epoch, config['identifier']))
        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}h {:.0f}m {:.0f}s'.format(time_elapsed // 3600, time_elapsed // 60 % 60, time_elapsed % 60))
    print('Best mean average precision @0.5: {:.4f}%'.format(100 * best_map_mean))

    # Load best model weights
    model.load_state_dict(best_model_weights)
    return model

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', default = 'pascal_voc_2007', type = str)
parser.add_argument('--network', default = 'resnet101_small', type = str)
args = parser.parse_args()

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
identifier = uuid.uuid4()
# Load dataset
if (args.dataset == 'pascal_voc_2007'):
    config = {
	'identifier': identifier,
        'dataset': args.dataset,
        'class_list':
            ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair',
             'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 
             'sheep', 'sofa', 'train', 'tvmonitor'],
        'anchor_sizes':
            ((128, 128), (128 * 2 ** 0.5, 128 / 2 ** 0.5), (128 / 2 ** 0.5, 128 * 2 ** 0.5),
             (256, 256), (256 * 2 ** 0.5, 256 / 2 ** 0.5), (256 / 2 ** 0.5, 256 * 2 ** 0.5),
             (512, 512), (512 * 2 ** 0.5, 512 / 2 ** 0.5), (512 / 2 ** 0.5, 512 * 2 ** 0.5)),
        'rpn_channels': 512,
        'rpn_kernel_size': 3,
        'rpn_batch_size': 256,
        'rpn_pos_iou': 0.7,
        'rpn_neg_iou': 0.3,
        'rpn_nms_iou_threshold': 0.7,
        'cls_iou_threshold': 0.5,
        'cls_iou_threshold_bg': 0.1,
        'cls_batch_size': 128,
        'mean_tfm': (0, 0, 0, 0),
        'std_tfm': (0.1, 0.1, 0.2, 0.2),
        'train_proposal_number': 2000,
        'val_proposal_number': 300,
        'lr': 0.001,
        'momentum': 0.9,
        'weight_decay': 0.0005,
        'num_epochs': 18,
        'lr_decay_epochs': 12,
        'gamma': 0.1,
        'mAP@': [0.5]
    }
    transform =  {'train':
        torchvision.transforms.Compose(
        (odd.Rescale(600, 1000),
        odd.FlipHorizontal(),
        odd.ToTensor(),
        odd.Normalize(mean = (0.485, 0.456, 0.406), std = (0.229, 0.224, 0.225)))),
                  'val':
        torchvision.transforms.Compose(
        (odd.Rescale(600, 1000),
        odd.ToTensor(),
        odd.Normalize(mean = (0.485, 0.456, 0.406), std = (0.229, 0.224, 0.225))))
        }
    voc_dir = './data/VOCdevkit/VOC2007'
    dataset = {}
    dataset['train'] = odd.load_pascal_voc(voc_dir, 'train', config['class_list'], transform['train'])
    dataset['val'] = odd.load_pascal_voc(voc_dir, 'val', config['class_list'], transform['train'])
    dataset['train'].include_data(dataset['val'].annotations, dataset['val'].images_paths)
    dataset['val'] = odd.load_pascal_voc(voc_dir, 'test', config['class_list'], transform['val'])
elif (args.dataset == 'pascal_voc_2012'):
    config = {
        'identifier': identifier,
        'dataset': args.dataset,
        'class_list':
            ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair',
             'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 
             'sheep', 'sofa', 'train', 'tvmonitor'],
        'anchor_sizes':
            ((128, 128), (128 * 2 ** 0.5, 128 / 2 ** 0.5), (128 / 2 ** 0.5, 128 * 2 ** 0.5),
             (256, 256), (256 * 2 ** 0.5, 256 / 2 ** 0.5), (256 / 2 ** 0.5, 256 * 2 ** 0.5),
             (512, 512), (512 * 2 ** 0.5, 512 / 2 ** 0.5), (512 / 2 ** 0.5, 512 * 2 ** 0.5)),
        'rpn_channels': 512,
        'rpn_kernel_size': 3,
        'rpn_batch_size': 256,
        'rpn_pos_iou': 0.7,
        'rpn_neg_iou': 0.3,
        'rpn_nms_iou_threshold': 0.7,
        'cls_iou_threshold': 0.5,
        'cls_iou_threshold_bg': 0.1,
        'cls_batch_size': 128,
        'mean_tfm': (0, 0, 0, 0),
        'std_tfm': (0.1, 0.1, 0.2, 0.2),
        'train_proposal_number': 2000,
        'val_proposal_number': 300,
        'lr': 0.001,
        'momentum': 0.9,
        'weight_decay': 0.0005,
        'num_epochs': 18,
        'lr_decay_epochs': 12,
        'gamma': 0.1,
        'mAP@': [0.5]
    }
    transform =  {'train':
        torchvision.transforms.Compose(
        (odd.Rescale(600, 1000),
        odd.FlipHorizontal(),
        odd.ToTensor(),
        odd.Normalize(mean = (0.485, 0.456, 0.406), std = (0.229, 0.224, 0.225)))),
                  'val':
        torchvision.transforms.Compose(
        (odd.Rescale(600, 1000),
        odd.ToTensor(),
        odd.Normalize(mean = (0.485, 0.456, 0.406), std = (0.229, 0.224, 0.225))))
        }
    voc_dir = './data/VOCdevkit/VOC2012'
    dataset = {x: odd.load_pascal_voc(voc_dir, x, config['class_list'], transform[x]) for x in ['train', 'val']}
elif (args.dataset == 'pascal_voc_2007_07+12'):
    config = {
	'identifier': identifier,
        'dataset': args.dataset,
        'class_list':
            ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', 'cat', 'chair',
             'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'pottedplant', 
             'sheep', 'sofa', 'train', 'tvmonitor'],
        'anchor_sizes':
            ((128, 128), (128 * 2 ** 0.5, 128 / 2 ** 0.5), (128 / 2 ** 0.5, 128 * 2 ** 0.5),
             (256, 256), (256 * 2 ** 0.5, 256 / 2 ** 0.5), (256 / 2 ** 0.5, 256 * 2 ** 0.5),
             (512, 512), (512 * 2 ** 0.5, 512 / 2 ** 0.5), (512 / 2 ** 0.5, 512 * 2 ** 0.5)),
        'rpn_channels': 512,
        'rpn_kernel_size': 3,
        'rpn_batch_size': 256,
        'rpn_pos_iou': 0.7,
        'rpn_neg_iou': 0.3,
        'rpn_nms_iou_threshold': 0.7,
        'cls_iou_threshold': 0.5,
        'cls_iou_threshold_bg': 0.1,
        'cls_batch_size': 128,
        'mean_tfm': (0, 0, 0, 0),
        'std_tfm': (0.1, 0.1, 0.2, 0.2),
        'train_proposal_number': 2000,
        'val_proposal_number': 300,
        'lr': 0.001,
        'momentum': 0.9,
        'weight_decay': 0.0005,
        'num_epochs': 12,
        'lr_decay_epochs': 6,
        'gamma': 0.1,
        'mAP@': [0.5]
    }
    transform =  {'train':
        torchvision.transforms.Compose(
        (odd.Rescale(600, 1000),
        odd.FlipHorizontal(),
        odd.ToTensor(),
        odd.Normalize(mean = (0.485, 0.456, 0.406), std = (0.229, 0.224, 0.225)))),
                  'val':
        torchvision.transforms.Compose(
        (odd.Rescale(600, 1000),
        odd.ToTensor(),
        odd.Normalize(mean = (0.485, 0.456, 0.406), std = (0.229, 0.224, 0.225))))
        }
    voc_dir = './data/VOCdevkit/VOC2012'
    dataset = {}
    dataset['train'] = odd.load_pascal_voc(voc_dir, 'train', config['class_list'], transform['train'])
    dataset_aux = odd.load_pascal_voc(voc_dir, 'val', config['class_list'], transform['train'])
    dataset['train'].include_data(dataset_aux.annotations, dataset_aux.images_paths)
    voc_dir = './data/VOCdevkit/VOC2007'
    dataset_aux = odd.load_pascal_voc(voc_dir, 'train', config['class_list'], transform['train'])
    dataset['train'].include_data(dataset_aux.annotations, dataset_aux.images_paths)
    dataset_aux = odd.load_pascal_voc(voc_dir, 'val', config['class_list'], transform['train'])
    dataset['train'].include_data(dataset_aux.annotations, dataset_aux.images_paths)
    dataset['val'] = odd.load_pascal_voc(voc_dir, 'test', config['class_list'], transform['val'])

elif (args.dataset == 'microsoft_coco_2017'):
    config = {
        'identifier': identifier,
        'dataset': args.dataset,
        'class_list': ('person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 
            'train', 'truck', 'boat', 'traffic light', 'fire hydrant', 'stop sign', 
            'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow', 
            'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 
            'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite', 
            'baseball bat', 'baseball glove', 'skateboard', 'surfboard', 'tennis racket', 
            'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 
            'apple', 'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 
            'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed', 'dining table', 
            'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone', 
            'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 
            'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush'),
        'anchor_sizes':
            ((64, 64), (64 * 2 ** 0.5, 64 / 2 ** 0.5), (64 / 2 ** 0.5, 64 * 2 ** 0.5),
             (128, 128), (128 * 2 ** 0.5, 128 / 2 ** 0.5), (128 / 2 ** 0.5, 128 * 2 ** 0.5),
             (256, 256), (256 * 2 ** 0.5, 256 / 2 ** 0.5), (256 / 2 ** 0.5, 256 * 2 ** 0.5),
             (512, 512), (512 * 2 ** 0.5, 512 / 2 ** 0.5), (512 / 2 ** 0.5, 512 * 2 ** 0.5)),
        'rpn_channels': 512,
        'rpn_kernel_size': 3,
        'rpn_batch_size': 256,
        'rpn_pos_iou': 0.7,
        'rpn_neg_iou': 0.3,
        'rpn_nms_iou_threshold': 0.7,
        'cls_iou_threshold': 0.5,
        'cls_iou_threshold_bg': 0.0,
        'cls_batch_size': 128,
        'mean_tfm': (0, 0, 0, 0),
        'std_tfm': (0.1, 0.1, 0.2, 0.2),
        'train_proposal_number': 2000,
        'val_proposal_number': 300,
        'lr': 0.001,
        'momentum': 0.9,
        'weight_decay': 0.0005,
        'num_epochs': 12,
        'lr_decay_epochs': 6,
        'gamma': 0.1,
        'mAP@': [0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95]
    }
    transform =  {'train':
        torchvision.transforms.Compose(
        (odd.Rescale(600, 1000),
        odd.FlipHorizontal(),
        odd.ToTensor(),
        odd.Normalize(mean = (0.485, 0.456, 0.406), std = (0.229, 0.224, 0.225)))),
                  'val':
        torchvision.transforms.Compose(
        (odd.Rescale(600, 1000),
        odd.ToTensor(),
        odd.Normalize(mean = (0.485, 0.456, 0.406), std = (0.229, 0.224, 0.225))))
        }
    coco_dir = './data/coco'
    dataset = {x: odd.load_microsoft_coco(coco_dir, x, config['class_list'], transform[x]) for x in ['train', 'val']}

print('Training dataset has {} images.'.format(len(dataset['train'])))
print('Validation dataset has {} images.'.format(len(dataset['val'])))

# Load model
if (args.network == 'resnet101_small'):
    config['base_model'] = 'resnet101_small'
    config['base_out_channels'] = 1024
    config['base_stride'] = 16
    config['roi_size'] = 7
    resnet = torchvision.models.resnet101(pretrained = True)
    def disable_batchnorm(m):
        if isinstance(m, torch.nn.BatchNorm2d):
            for p in m.parameters():
                p.requires_grad = False
    resnet.apply(disable_batchnorm)
    for p in resnet.conv1.parameters():
        p.requires_grad = False
    model_base = torch.nn.Sequential(*list(resnet.children())[:7])
    model_head = torch.nn.Sequential(*list(resnet.children())[7], torch.nn.AvgPool2d(4), torch.nn.Conv2d(2048, len(config['class_list']) * 5 + 5, 1))
    model = faster_rcnn.FasterRCNN(model_base, model_head, config)
    model.to(device)

# Train model
optimizer = torch.optim.SGD(filter(lambda p: p.requires_grad, model.parameters()), lr = config['lr'], momentum = config['momentum'], weight_decay = config['weight_decay'])
# Decay LR by a factor of 0.1 every 7 epochs
exp_lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size = config['lr_decay_epochs'], gamma = config['gamma'])
model = train_faster_rcnn(model, optimizer, exp_lr_scheduler, dataset, config)
torch.save({'model': model.state_dict(), 'config': config}, './data/weights/faster_rcnn_{}_{}_best_{}.pth'.format(config['base_model'], config['dataset'], config['identifier']))
