import os
import json

def save_pascal_voc(output_path, bbox_list, prediction_list, confidence_list, images_ids, class_list):
    for i in range(len(class_list)):
        if (not os.path.isdir(output_path)):
            os.makedirs(output_path)
        with open(output_path + 'comp3_det_test_' + class_list[i] + '.txt', 'w') as f:
            for j in range(len(bbox_list)):
                bbox = bbox_list[j]
                conf = confidence_list[j]
                pred = prediction_list[j]
                for k in range(pred.shape[0]):
                    if (pred[k] == i):
                        f.write(' '.join((images_ids[j], str(conf[k]), str(bbox[k, 0]), str(bbox[k, 1]), str(bbox[k, 0] + bbox[k, 2]), str(bbox[k, 1] + bbox[k, 3]))) + '\n')

def save_coco(output_path, detection_list):
    with open(output_path, 'w') as f:
        json.dump(detection_list, f)
