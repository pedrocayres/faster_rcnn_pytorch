import torch
import lib.object_detection_dataset as odd

def intersection_over_union(bbox_1, bbox_2):
    bbox_1_x_min = bbox_1[0].view(-1, 1)
    bbox_1_y_min = bbox_1[1].view(-1, 1)
    bbox_1_x_max = (bbox_1[0] + bbox_1[2]).view(-1, 1)
    bbox_1_y_max = (bbox_1[1] + bbox_1[3]).view(-1, 1)
    bbox_1_area = (bbox_1[2] * bbox_1[3]).view(-1, 1)
    bbox_2_x_min = bbox_2[0].view(1, -1)
    bbox_2_y_min = bbox_2[1].view(1, -1)
    bbox_2_x_max = (bbox_2[0] + bbox_2[2]).view(1, -1)
    bbox_2_y_max = (bbox_2[1] + bbox_2[3]).view(1, -1)
    bbox_2_area = (bbox_2[2] * bbox_2[3]).view(1, -1)
    bbox_intersection_x = torch.max(bbox_1_x_min, bbox_2_x_min)
    bbox_intersection_y = torch.max(bbox_1_y_min, bbox_2_y_min)
    bbox_intersection_w = (torch.min(bbox_1_x_max, bbox_2_x_max) - bbox_intersection_x).clamp(min = 0)
    bbox_intersection_h = (torch.min(bbox_1_y_max, bbox_2_y_max) - bbox_intersection_y).clamp(min = 0)
    intersection = bbox_intersection_w * bbox_intersection_h
    union = bbox_1_area + bbox_2_area - intersection
    return intersection / union

def non_maximum_suppression(score, bbox, iou_threshold):
    score, indices = score.sort(0, descending = True)
    bbox = bbox[:, indices]
    filter_ind = torch.ones((score.size(0), score.size(0)), dtype = torch.uint8, device = score.device)
    loop_size = score.size(0) // 1000
    for i in range(loop_size):
        iou_block = intersection_over_union(bbox[:, (1000 * i):(1000 * (i + 1))], bbox[:, (1000 * i):])
        filter_ind[(1000 * i):(1000 * (i + 1)), (1000 * i):] = (iou_block < iou_threshold)
    if (1000 * loop_size != score.size(0)):
        iou_block = intersection_over_union(bbox[:, (1000 * loop_size):], bbox[:, (1000 * loop_size):])
        filter_ind[(1000 * loop_size):, (1000 * loop_size):] = (iou_block < iou_threshold)
    aux = filter_ind[:, ::8]
    for i in range(1, min(8, filter_ind.size(1))):
        aux_2 = filter_ind[:, i::8] << i
        aux[:, :aux_2.size(1)] += aux_2
    aux = aux.cpu().numpy()
    indices_2 = [0]
    for i in range(1, aux.shape[0]):
        j = i // 8
        k = i % 8
        if (aux[0, j] & (1 << k)):
            indices_2.append(i)
            aux[0, j:] &= aux[i, j:]
    indices_2 = torch.tensor(indices_2)
    return score[indices_2], bbox[:, indices_2]

def average_precision(confidence_list, prediction_list, bbox_list, annotations, difficult_list, class_list, iou_threshold, device, voc_2007_map = False):
    torch.set_grad_enabled(False)
    n_pos = torch.zeros(len(class_list), device = device)
    conf_list, tp_list = [], []
    for i in range(len(class_list)):
        conf_list.append(torch.tensor([], device = device))
        tp_list.append(torch.tensor([], device = device))
    for n in range(len(annotations)):
        labels = torch.tensor(annotations[n]).to(device).float()
        confidence = torch.tensor(confidence_list[n]).to(device)
        prediction = torch.tensor(prediction_list[n]).to(device)
        final_bbox = torch.tensor(bbox_list[n]).to(device)
        difficult = torch.tensor(difficult_list[n]).to(device).long()

        # Count ground truths occurences for each class
        for i, gt in enumerate(labels):
            if (difficult[i] == 0):
                n_pos[int(gt[0].item())] += 1

        # Find true positives and false positives for every class in this image
        conf_list_2, tp_list_2 = [], []
        for i in range(len(class_list)):
            conf_list_2.append(confidence[prediction == i])
            tp_list_2.append(torch.zeros(conf_list_2[-1].size(0), len(iou_threshold), device = device))
        if (len(final_bbox) > 0):
            for i in range(len(class_list)):
                labels_i = labels[labels[:, 0] == i, :]
                difficult_i = difficult[labels[:, 0] == i]
                final_bbox_i = final_bbox[prediction == i, :]
                if (len(final_bbox_i) == 0 or len(labels_i) == 0):
                    continue
                iou = intersection_over_union(final_bbox_i.transpose(0, 1), labels_i[:, 1:].transpose(0, 1))
                iou_max, j_max = torch.max(iou, dim = 1)
                for k in range(len(iou_threshold)):
                    j_max[iou_max < iou_threshold[k]] = -1
                    for j in range(labels_i.size(0)):
                        if (difficult_i[j] == 1):
                           tp_list_2[i][j_max == j, k] = 2
                           continue 
                        pos_conf = conf_list_2[i] - torch.min(conf_list_2[i]) + 1
                        pos_conf_max, arg_max = torch.max(pos_conf * (j_max == j).float(), dim = 0)
                        if (pos_conf_max > 0):
                            tp_list_2[i][arg_max, k] = 1
        for i in range(len(class_list)):
            tp_list[i] = torch.cat((tp_list[i], tp_list_2[i]), dim = 0)
            conf_list[i] = torch.cat((conf_list[i], conf_list_2[i]))
 
    # Calculate average precision for every class
    ap = torch.zeros(len(class_list), len(iou_threshold), device = device)
    for i in range(len(class_list)):
        cls_conf_list = conf_list[i]
        cls_tp_list = tp_list[i]
        if (cls_tp_list.size(0) != 0 and n_pos[i] != 0):
            # Sort detections by confidence
            cls_conf_list, indices = cls_conf_list.sort(descending = True)
            cls_tp_list = cls_tp_list[indices, :]
            cls_fp_list = 1 - cls_tp_list
            cls_fp_list[cls_tp_list == 2] = 0
            cls_tp_list[cls_tp_list == 2] = 0

            # Compute precision and recall
            tp = torch.cumsum(cls_tp_list, dim = 0)
            fp = torch.cumsum(cls_fp_list, dim = 0)
            rec = tp / n_pos[i]
            prec = tp / torch.clamp(fp + tp, min = 1)

            # Calculate average precision
            if (voc_2007_map):
                for j in range(len(iou_threshold)):
                    for t in range(11):
                        if (torch.sum(rec[:, j] >= t / 10) == 0):
                            p = 0
                        else:
                            p = torch.max(prec[:, j][rec[:, j] >= t / 10])
                        ap[i, j] += p / 11
            else:
                mrec = torch.zeros(rec.size(0) + 2, rec.size(1), device = device)
                mrec[1:(rec.size(0) + 1), :] = rec
                mrec[rec.size(0) + 1, :] = 1
                mpre = torch.zeros(prec.size(0) + 2, prec.size(1), device = device)
                mpre[1:(prec.size(0) + 1), :] = prec
                for j in range(mpre.size(0) - 2, -1, -1):
                    mpre[j, :] = torch.max(mpre[j, :], mpre[j + 1, :])
                ap[i, :] = torch.sum((mrec[1:mrec.size(0), :] - mrec[0:(mrec.size(0) - 1), :]) * mpre[1:mpre.size(0), :], dim = 0)
    return ap
