import torch
from lib.bbox_utils import transform_bbox, compute_bbox_tfm_target, clip_bbox
from lib.roi_align.roi_align import ROIAlign
from lib.metrics import intersection_over_union, non_maximum_suppression
import math

class FasterRCNN_FPN(torch.nn.Module):
    def __init__(self, base, head, config):
        super(FasterRCNN_FPN, self).__init__()
        self.base = base
        self.head = head
        self.config = config
        self.roi_align_layer = []
        for i in range(len(config['base_stride']) - 1):
            self.roi_align_layer.append(ROIAlign((self.config['roi_size'], self.config['roi_size']), 1.0 / self.config['base_stride'][i], 2))
        self.rpn_conv = torch.nn.Conv2d(self.config['base_out_channels'], self.config['rpn_channels'], self.config['rpn_kernel_size'], padding = 1)
        self.rpn_relu = torch.nn.ReLU(inplace = True)
        self.rpn_conv_cls = torch.nn.Conv2d(self.config['rpn_channels'], 2 * 3, 1)
        self.rpn_conv_tfm = torch.nn.Conv2d(self.config['rpn_channels'], 4 * 3, 1)
        self.cross_entropy = torch.nn.CrossEntropyLoss(reduction = 'sum')
        self.smooth_l1_loss = torch.nn.SmoothL1Loss(reduction = 'sum')
        normal_init(self.rpn_conv, 0, 0.01)
        normal_init(self.rpn_conv_cls, 0, 0.01)
        normal_init(self.rpn_conv_tfm, 0, 0.01)

    def train(self, mode = True):
        torch.nn.Module.train(self, mode)
        def disable_batchnorm(m):
            if isinstance(m, torch.nn.BatchNorm2d):
                m.eval()
        self.apply(disable_batchnorm)

    def sample_cls_targets(self, proposal_bbox, gt_cls, gt_bbox):
        # Augment training roi bounding boxes with ground-truth bounding boxes
        proposal_bbox = torch.cat((proposal_bbox, gt_bbox.transpose(1, 0)))
        # Define targets for the classification layer
        gt_proposal_iou = intersection_over_union(proposal_bbox.transpose(1, 0), gt_bbox)
        j = torch.argmax(gt_proposal_iou, dim = 1)
        i = torch.arange(0, proposal_bbox.size(0), dtype = torch.long, device = proposal_bbox.device)
        cls_indices = torch.stack((i, j, gt_cls[j]), dim = 1)
        aux = ((gt_proposal_iou[i, j] < self.config['cls_iou_threshold']) & (gt_proposal_iou[i, j] >= self.config['cls_iou_threshold_bg'])).nonzero().view(-1)
        if (len(aux) > 0):
            bg_indices = cls_indices[aux, 0].view(-1)
        else:
            bg_indices = torch.tensor([], dtype = torch.long, device = proposal_bbox.device)
        aux = (gt_proposal_iou[i, j] >= self.config['cls_iou_threshold']).nonzero().view(-1)
        if (len(aux) > 0):
            cls_indices = cls_indices[aux, :]
        else:
            cls_indices = torch.tensor([], dtype = torch.long, device = proposal_bbox.device)

        # Sample bounding-boxes with objects to be classified and their respective targets
        perm = torch.randperm(len(cls_indices))[0:min(len(cls_indices), int(self.config['cls_batch_size'] * self.config['fg_ratio']))]
        if (len(perm) > 0):
            cls_indices = cls_indices[perm, :]
            proposal_bbox_cls = proposal_bbox[cls_indices[:, 0], :]
            target_bbox_tfm = compute_bbox_tfm_target(proposal_bbox_cls, gt_bbox[:, cls_indices[:, 1]].transpose(0, 1))
            target_bbox_tfm[:, 0] = (target_bbox_tfm[:, 0] - self.config['mean_tfm'][0]) / self.config['std_tfm'][0]
            target_bbox_tfm[:, 1] = (target_bbox_tfm[:, 1] - self.config['mean_tfm'][1]) / self.config['std_tfm'][1]
            target_bbox_tfm[:, 2] = (target_bbox_tfm[:, 2] - self.config['mean_tfm'][2]) / self.config['std_tfm'][2]
            target_bbox_tfm[:, 3] = (target_bbox_tfm[:, 3] - self.config['mean_tfm'][3]) / self.config['std_tfm'][3]
            target_bbox_tfm = target_bbox_tfm.detach()
            target_cls = cls_indices[:, 2]
        else:
            proposal_bbox_cls = torch.tensor([], device = proposal_bbox.device)
            target_cls = torch.tensor([], dtype = torch.long, device = proposal_bbox.device)
            target_bbox_tfm = torch.tensor([], device = proposal_bbox.device)
        perm = torch.randperm(len(bg_indices))[0:min(len(bg_indices), self.config['cls_batch_size'] - len(cls_indices))]
        if (len(perm) > 0):
            bg_indices = bg_indices[perm]
            proposal_bbox_bg = proposal_bbox[bg_indices, :]
            target_bg = len(self.config['class_list']) * torch.ones(len(perm), dtype = torch.long, device = proposal_bbox.device)
        else:
            proposal_bbox_bg = torch.tensor([], device = proposal_bbox.device)
            target_bg = torch.tensor([], dtype = torch.long, device = proposal_bbox.device)
        target_cls = torch.cat((target_cls, target_bg))
        proposal_bbox = torch.cat((proposal_bbox_cls, proposal_bbox_bg))
        return proposal_bbox, target_cls, target_bbox_tfm
 
    def sample_rpn_loss(self, anchor_bbox, rpn_cls, rpn_bbox_tfm, gt_bbox, image_size):
        #if (self.training):
            # If anchor isn't within bounds, exclude it from training
        #    filter_mask = ((anchor_bbox[:, 0] >= 0) & (anchor_bbox[:, 1] >= 0) & (anchor_bbox[:, 0] + anchor_bbox[:, 2] < image_size[3]) & (anchor_bbox[:, 1] + anchor_bbox[:, 3] < image_size[2]))
        #    anchor_bbox = anchor_bbox[filter_mask, :]
        #    rpn_cls = rpn_cls[filter_mask, :]
        #    rpn_bbox_tfm = rpn_bbox_tfm[filter_mask, :]

        # Computes IoU between ground-truth objects and anchors
        gt_anchor_iou = intersection_over_union(gt_bbox, anchor_bbox.transpose(1, 0))

        # Check which anchors have sufficient IoU with ground-truth objects
        prop_iou_max, prop_best_match = torch.max(gt_anchor_iou, dim = 0)
        pos_ind = (prop_iou_max >= self.config['rpn_pos_iou'])
        prop_best_match[1 - pos_ind] = -1
        gt_iou_max, gt_iou_argmax = torch.max(gt_anchor_iou, dim = 1)
        pos_ind[gt_iou_argmax[gt_iou_max > 0]] = 1
        neg_ind = (prop_iou_max < self.config['rpn_neg_iou']) & (1 - pos_ind)
        prop_best_match[gt_iou_argmax[gt_iou_max > 0]] = torch.arange(0, gt_iou_max.size(0), dtype = torch.long, device = rpn_cls.device)[gt_iou_max > 0]
        anchor_neg_indices = neg_ind.nonzero().view(-1)
        anchor_pos_indices = pos_ind.nonzero().view(-1)
        prop_best_match = prop_best_match[prop_best_match >= 0]

        # Cross entropy loss and bounding-box transform loss for proposals
        if (len(anchor_pos_indices) != 0):
            if (anchor_pos_indices.size(0) > (self.config['rpn_batch_size'] // 2)):  
                perm = torch.randperm(anchor_pos_indices.size(0))[0:(self.config['rpn_batch_size'] // 2)]
                anchor_pos_indices = anchor_pos_indices[perm]
                prop_best_match = prop_best_match[perm]
            gt_bbox_tfm = compute_bbox_tfm_target(anchor_bbox[anchor_pos_indices, :], gt_bbox[:, prop_best_match].transpose(0, 1))
            loss_rpn_cls = self.cross_entropy(rpn_cls[anchor_pos_indices, :], torch.zeros(anchor_pos_indices.size(0), dtype = torch.long, device = rpn_cls.device))
            loss_rpn_tfm = self.smooth_l1_loss(9 * rpn_bbox_tfm[anchor_pos_indices, :], 9 * gt_bbox_tfm) / 9
        else:
            loss_rpn_cls = torch.zeros(1, device = rpn_cls.device)
            loss_rpn_tfm = torch.zeros(1, device = rpn_cls.device)
        if (len(anchor_neg_indices) != 0):
            if (anchor_neg_indices.size(0) > self.config['rpn_batch_size'] - len(anchor_pos_indices)):
                perm = torch.randperm(anchor_neg_indices.size(0))[0:(self.config['rpn_batch_size'] - len(anchor_pos_indices))]
                anchor_neg_indices = anchor_neg_indices[perm]
            loss_rpn_cls = loss_rpn_cls + self.cross_entropy(rpn_cls[anchor_neg_indices, :], torch.ones(anchor_neg_indices.size(0), dtype = torch.long, device = rpn_cls.device))
        loss_rpn_cls /= len(anchor_pos_indices) + len(anchor_neg_indices)
        loss_rpn_tfm /= len(anchor_pos_indices) + len(anchor_neg_indices)
        return loss_rpn_cls, loss_rpn_tfm

    def rpn(self, input_features, gt_bbox, image_size):
        anchor_bbox_all = torch.tensor([], device = input_features[0].device)
        rpn_cls_all = torch.tensor([], device = input_features[0].device)
        rpn_bbox_tfm_all  = torch.tensor([], device = input_features[0].device)
        obj_score  = torch.tensor([], device = input_features[0].device)
        roi_bbox = torch.tensor([], device = input_features[0].device)
        for i in range(len(input_features)):
            # Calculate intermediate features then classify each anchor and estimate bounding-box transform
            rpn_features = self.rpn_relu(self.rpn_conv(input_features[i]))
            rpn_cls = self.rpn_conv_cls(rpn_features)
            rpn_bbox_tfm = self.rpn_conv_tfm(rpn_features)
    
            anchor_sizes = torch.tensor(self.config['anchor_sizes'][(3 * i):(3 * i + 3)], dtype = torch.float, device = input_features[i].device)
            # Calculate anchors' bounding-boxes
            x_grid = 0.5 * self.config['base_stride'][i] + (torch.arange(rpn_cls.size(3), dtype = torch.float, device = input_features[i].device) * self.config['base_stride'][i]).view(1, 1, -1).repeat(anchor_sizes.size(0), rpn_cls.size(2), 1)
            y_grid = 0.5 * self.config['base_stride'][i] + (torch.arange(rpn_cls.size(2), dtype = torch.float, device = input_features[i].device) * self.config['base_stride'][i]).view(1, -1, 1).repeat(anchor_sizes.size(0), 1, rpn_cls.size(3))
            anchor_bbox_x = x_grid - anchor_sizes[:, 0].view(-1, 1, 1).repeat(1, x_grid.size(1), x_grid.size(2)) * 0.5
            anchor_bbox_y = y_grid - anchor_sizes[:, 1].view(-1, 1, 1).repeat(1, y_grid.size(1), y_grid.size(2)) * 0.5
            anchor_bbox_w = anchor_sizes[:, 0].view(-1, 1, 1).repeat(1, x_grid.size(1), x_grid.size(2))
            anchor_bbox_h = anchor_sizes[:, 1].view(-1, 1, 1).repeat(1, y_grid.size(1), y_grid.size(2))
            anchor_bbox = torch.stack((anchor_bbox_x, anchor_bbox_y, anchor_bbox_w, anchor_bbox_h), dim = 0)
            anchor_bbox = anchor_bbox.permute(2, 3, 1, 0).contiguous().view(-1, 4)
            rpn_cls = rpn_cls.permute(0, 2, 3, 1).contiguous().view(-1, 2)
            rpn_bbox_tfm = rpn_bbox_tfm.permute(0, 2, 3, 1).contiguous().view(-1, 4)

            # Accumulate over all levels
            anchor_bbox_all = torch.cat((anchor_bbox_all, anchor_bbox), dim = 0)
            rpn_cls_all = torch.cat((rpn_cls_all, rpn_cls), dim = 0)
            rpn_bbox_tfm_all = torch.cat((rpn_bbox_tfm_all, rpn_bbox_tfm), dim = 0)
    
            # Calculate rois from anchors and bounding-box transformation parameters
            roi_bbox_i = transform_bbox(anchor_bbox, rpn_bbox_tfm)
            obj_score_i = (torch.exp(rpn_cls[:, 0::2]) / (torch.exp(rpn_cls[:, 1::2]) + torch.exp(rpn_cls[:, 0::2]))).view(-1)
    
            # Ensure roi is within the image's bounds
            roi_bbox_i = clip_bbox(roi_bbox_i, image_size[3], image_size[2])

            # Filter proposals with width or height too small
            filter_mask = (roi_bbox_i[:, 2] >= 1) & (roi_bbox_i[:, 3] >= 1)
            obj_score_i = obj_score_i[filter_mask]
            roi_bbox_i = roi_bbox_i[filter_mask, :]

            # Filter low ranked proposals (pre-nms)
            obj_score_i, indices = obj_score_i.sort(0, descending = True)
            roi_bbox_i = roi_bbox_i[indices, :]
            if (self.training):
                obj_score_i = obj_score_i[:min(obj_score_i.size(0), self.config['train_pre_nms_proposal_number'])]
                roi_bbox_i = roi_bbox_i[:min(roi_bbox_i.size(0), self.config['train_pre_nms_proposal_number']), :]
            else:
                obj_score_i = obj_score_i[:min(obj_score_i.size(0), self.config['val_pre_nms_proposal_number'])]
                roi_bbox_i = roi_bbox_i[:min(roi_bbox_i.size(0), self.config['val_pre_nms_proposal_number']), :]

            # Rank objects by confidence score
            with torch.set_grad_enabled(False):
                obj_score_i, roi_bbox_i = non_maximum_suppression(obj_score_i, roi_bbox_i.transpose(1, 0), self.config['rpn_nms_iou_threshold'])
                roi_bbox_i = roi_bbox_i.transpose(1, 0)

            # Take some of the top proposals, ranked by confidence score
            if (self.training):
                roi_bbox_i = roi_bbox_i[:min(roi_bbox_i.size(0), self.config['train_proposal_number']), :]
                obj_score_i = obj_score_i[:min(obj_score_i.size(0), self.config['train_proposal_number'])]
            else:
                roi_bbox_i = roi_bbox_i[:min(roi_bbox_i.size(0), self.config['val_proposal_number']), :]
                obj_score_i = obj_score_i[:min(obj_score_i.size(0), self.config['val_proposal_number'])]

            roi_bbox = torch.cat((roi_bbox, roi_bbox_i), dim = 0)
            obj_score = torch.cat((obj_score, obj_score_i), dim = 0)

        if (self.training):
            # Sample "object" and "non-object" anchors to compute RPN loss
            loss_rpn_cls, loss_rpn_tfm = self.sample_rpn_loss(anchor_bbox_all, rpn_cls_all, rpn_bbox_tfm_all, gt_bbox, image_size)
        else:
            loss_rpn_cls = torch.zeros(1, device = input_features[0].device)
            loss_rpn_tfm = torch.zeros(1, device = input_features[0].device)

        obj_score, indices = obj_score.sort(0, descending = True)
        roi_bbox = roi_bbox[indices, :]
 
        # Take some of the top proposals, ranked by confidence score
        if (self.training):
            roi_bbox = roi_bbox[:min(roi_bbox.size(0), self.config['train_proposal_number']), :]
        else:
            roi_bbox = roi_bbox[:min(roi_bbox.size(0), self.config['val_proposal_number']), :]
        return rpn_cls, rpn_bbox_tfm, loss_rpn_cls, loss_rpn_tfm, roi_bbox

    def forward(self, input_features, actual_size = None, labels = None):
        if (self.training):
            # Ground-truth classes and bounding-boxes
            gt_bbox = labels[:, :, 1:].transpose(1, 2)
            gt_cls = labels[:, :, 0].long()
        else:
            gt_bbox = torch.ones(1, 1, 1)
            gt_cls = torch.ones(1, 1)
        pad_2 = math.ceil(actual_size[2] / 32) * 32 - input_features.size(2)
        pad_3 = math.ceil(actual_size[3] / 32) * 32 - input_features.size(3)
        input_features = torch.nn.functional.pad(input_features, (0, pad_3, 0, pad_2), mode = 'constant', value = 0)
        actual_size = input_features.size()
        loss_cls = torch.zeros(input_features.size(0), device = input_features.device)
        loss_cls_tfm = torch.zeros(input_features.size(0), device = input_features.device)
        loss_rpn_cls = torch.zeros(input_features.size(0), device = input_features.device)
        loss_rpn_tfm = torch.zeros(input_features.size(0), device = input_features.device)
        cls_all = torch.tensor([], device = input_features.device)
        roi_bbox_all =  torch.tensor([], device = input_features.device)
        bbox_tfm_all =  torch.tensor([], device = input_features.device)

        for i in range(input_features.size(0)):
            feature_map = self.base(input_features[i, :, :, :].unsqueeze(0))

            # Calculate roi with RPN
            rpn_cls, rpn_bbox_tfm, loss_rpn_cls[i], loss_rpn_tfm[i], roi_bbox = self.rpn(feature_map, gt_bbox[i, :, :], actual_size)
            if (self.training):
                # Define roi and targets that will be used to obtain the classifier's loss
                roi_bbox, target_cls, target_bbox_tfm = self.sample_cls_targets(roi_bbox, gt_cls[i, :], gt_bbox[i, :, :])
                if (roi_bbox.numel() == 0):
                    continue
                roi_bbox = roi_bbox.view(-1, 4)
                target_cls = target_cls.view(-1)
                target_bbox_tfm = target_bbox_tfm.view(-1, 4) 

            # Extract the object's features from roi
            i_2 = torch.tensor(i, device = input_features.device).float()
            roi = torch.stack((i_2.expand(roi_bbox.size(0)), roi_bbox[:, 0], roi_bbox[:, 1], roi_bbox[:, 0] + roi_bbox[:, 2], roi_bbox[:, 1] + roi_bbox[:, 3]), dim = 1)
            object_features = torch.zeros(roi_bbox.size(0), self.config['base_out_channels'], self.config['roi_size'], self.config['roi_size'], device = input_features.device)
            area = roi_bbox[:, 2] * roi_bbox[:, 3]
            pyramid_level = torch.floor(2 + torch.log2(area / 50176) / 2).clamp(0, len(feature_map) - 2)
            for j in range(len(feature_map) - 1):
                mask = (pyramid_level == j)
                if (mask.sum() > 0):
                    object_features[mask, :, :, :] = self.roi_align_layer[j](feature_map[j], roi[mask, :])

            # Classify objects and compute bounding-box adjusting parameters
            cls, bbox_tfm = self.head(object_features)
            all_ind = torch.arange(bbox_tfm.size(0), dtype = torch.long, device = input_features.device)

            if (self.training):
                # Cross entropy loss and bounding-box transform loss for classification
                if (len(target_cls) > 0):
                    loss_cls[i] = self.cross_entropy(cls, target_cls) / len(target_cls)
                else:
                    loss_cls[i] = 0
                if (len(target_bbox_tfm) > 0):
                    tfm_aux = bbox_tfm.view(bbox_tfm.size(0), -1, 4)
                    loss_cls_tfm[i] = self.smooth_l1_loss(tfm_aux[all_ind[:target_bbox_tfm.size(0)], target_cls[:target_bbox_tfm.size(0)], :], target_bbox_tfm) / len(target_cls)
                else:
                    loss_cls_tfm[i] = 0

            # Obtain denormalized bounding box transformation coeficients
            bbox_tfm_den = bbox_tfm.clone()
            bbox_tfm_den[:, 0::4] = bbox_tfm[:, 0::4] * self.config['std_tfm'][0] + self.config['mean_tfm'][0]
            bbox_tfm_den[:, 1::4] = bbox_tfm[:, 1::4] * self.config['std_tfm'][1] + self.config['mean_tfm'][1]
            bbox_tfm_den[:, 2::4] = bbox_tfm[:, 2::4] * self.config['std_tfm'][2] + self.config['mean_tfm'][2]
            bbox_tfm_den[:, 3::4] = bbox_tfm[:, 3::4] * self.config['std_tfm'][3] + self.config['mean_tfm'][3]

            # Accumulate outputs over all images in the mini-batch
            roi_bbox_all = torch.cat((roi_bbox_all, torch.cat((i_2.expand(roi_bbox.size(0), 1), roi_bbox), dim = 1)))
            bbox_tfm_all = torch.cat((bbox_tfm_all, torch.cat((i_2.expand(bbox_tfm_den.size(0), 1), bbox_tfm_den), dim = 1)))
            cls_all = torch.cat((cls_all, torch.cat((i_2.expand(cls.size(0), 1), cls), dim = 1)))
        return roi_bbox_all.detach(), bbox_tfm_all.detach(), cls_all.detach(), loss_rpn_cls, loss_rpn_tfm, loss_cls, loss_cls_tfm

def normal_init(layer, mean, std):
    layer.weight.data.normal_(mean, std)
    layer.bias.data.zero_()
