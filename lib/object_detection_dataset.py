import torch
import torchvision.transforms.functional as F
import os
import sklearn
from skimage import io, transform
import numpy
import xml.etree.ElementTree as ET
import json
import operator

class ObjectDetectionDataset(torch.utils.data.Dataset):
    def __init__(self, annotations, images_paths, class_list, transform = None, difficult_list = None):
        """
        Args:
            annotations (list of numpy arrays): Annotations of all images.
                Each row of an entry is an array that corresponds to an object in
                the image. These rows have the form [c, x, y, w, h], where c is
                the object's class index, (x, y) is the upper-left point of the
                bounding-box, w is its width and h is its height.
            images_paths (list of string): Paths of all images.
            class_list (list of strings): Names of all possible classes.
            difficult_list (list of arrays): 1 if the object is deemed difficult to detect, 0 otherwise.
            transform (callable, optional): Optional transform to be applied on a sample.
        """
        self.images_paths = images_paths
        self.class_list = class_list
        self.transform = transform
        self.annotations = annotations
        if (difficult_list):
            self.difficult_list = difficult_list
        else:
            self.difficult_list = []
            for i in range(len(self.annotations)):
                self.difficult_list.append(numpy.zeros(self.annotations[i].shape[0], dtype = numpy.int64))

    def include_data(self, annotations, images_paths, difficult_list = None):
        self.annotations += annotations
        self.images_paths += images_paths
        if (difficult_list):
            self.difficult_list += difficult_list
        else:
            for i in range(len(annotations)):
                self.difficult_list.append(numpy.zeros(annotations[i].shape[0], dtype = numpy.int64))
            
    def shuffle(self):
        self.images_paths, self.annotations, self.difficult_list = sklearn.utils.shuffle(self.images_paths, self.annotations, self.difficult_list)

    def __len__(self):
        return len(self.annotations)

    def __getitem__(self, idx):
        image = io.imread(self.images_paths[idx]) / 255
        if (len(image.shape) == 2):
            image = numpy.repeat(image[:, :, numpy.newaxis], 3, axis = 2)
        sample = {'image': image, 'annotations': self.annotations[idx], 'scale': (1.0, 1.0)}
        if (self.transform):
            sample = self.transform(sample)
        return sample

def extract_coco_data(annotations_file, images_dir, class_list):
    """
    Args:
        annotations_file (string): Path to the file with annotations.
        images_dir (string): Path to the directory with the images.
        class_list (list of strings): Names of all possible classes.
    """
    data = json.load(open(annotations_file, 'r'))
    class_list_id = []
    for i in range(len(class_list)):
        for j in range(len(data['categories'])):
            if (class_list[i] == data['categories'][j]['name']):
                class_list_id.append(data['categories'][j]['id'])
                break
    data['images'].sort(key = operator.itemgetter('id'))
    data['annotations'].sort(key = operator.itemgetter('image_id'))
    annotations, images_paths = [], []
    last_i = 0
    for image_info in data['images']:
        aux = []
        for i in range(last_i, len(data['annotations'])):
            ann = data['annotations'][i]
            if (ann['image_id'] == image_info['id']):
                if (ann['iscrowd'] == 0):
                    for j in range(len(class_list_id)):
                        if (class_list_id[j] == ann['category_id']):
                            aux.append([j] + data['annotations'][i]['bbox'])
                            break
            else:
                last_i = i
                break
        if (len(aux) > 0):
            annotations.append(numpy.array(aux))
            images_paths.append(os.path.join(images_dir, image_info['file_name']))
    return annotations, images_paths

def load_microsoft_coco(coco_dir, dataset_type, class_list, transform = None):
    """
    Args:
        coco_dir (string): Path to the main Microsoft COCO directory.
        dataset_type (string): Type of dataset, i.e. train, val or test.
        class_list (list of strings): Names of all possible classes.
        transform (callable, optional): Optional transform to be applied on a sample.
    """
    annotations_file = os.path.join(coco_dir, 'annotations', 'instances_{}2017.json'.format(dataset_type))
    image_dir = os.path.join(coco_dir, 'images')
    annotations, images_paths = extract_coco_data(annotations_file, image_dir, class_list)
    dataset = ObjectDetectionDataset(annotations, images_paths, class_list, transform)
    return dataset

def extract_voc_data(annotations_dir, images_dir, file_list, class_list):
    """
    Args:
        annotations_dir (string): Path to the directory with annotations.
        images_dir (string): Path to the directory with the images.
        file_list (list of strings): Names of images and annotations' files.
        class_list (list of strings): Names of all possible classes.
    """
    annotations, images_paths, difficult_list = [], [], []
    for file_name in file_list:
        aux = []
        difficult = []
        file_path = os.path.join(annotations_dir, file_name + '.xml')
        tree = ET.parse(file_path)
        root = tree.getroot()
        for child in root:
            if child.tag == 'object':
                aux.append([-1, 0, 0, 0, 0])
                difficult.append(0)
                for c in child:
                    if (c.tag == 'name'):
                        for i in range(len(class_list)):
                            if (c.text == class_list[i]):
                                aux[-1][0] = i
                                break
                    elif (c.tag == 'bndbox'):
                        for d in c:
                            if (d.tag == 'xmin'):
                                aux[-1][1] = int(d.text)
                            elif(d.tag == 'ymin'):
                                aux[-1][2] = int(d.text)
                            elif(d.tag == 'xmax'):
                                aux[-1][3] = int(d.text)
                            elif(d.tag == 'ymax'):
                                aux[-1][4] = int(d.text)
                    elif (c.tag == 'difficult'):
                        difficult[-1] = int(c.text)
                aux[-1][3] = aux[-1][3] - aux[-1][1]
                aux[-1][4] = aux[-1][4] - aux[-1][2]
                if (aux[-1][0] == -1):
                    aux.pop()
                    difficult.pop()
        if (len(aux) > 0):
            annotations.append(numpy.array(aux))
            difficult_list.append(numpy.array(difficult, dtype = numpy.int64))
            images_paths.append(os.path.join(images_dir, file_name + '.jpg'))
    return annotations, images_paths, difficult_list

def load_pascal_voc(voc_dir, dataset_type, class_list, transform = None):
    """
    Args:
        voc_dir (string): Path to the main PASCAL VOC directory.
        dataset_type (string): Type of dataset, i.e. train, val or test.
        class_list (list of strings): Names of all possible classes.
        transform (callable, optional): Optional transform to be applied on a sample.
    """
    images_dir = os.path.join(voc_dir, 'JPEGImages')
    annotations_dir = os.path.join(voc_dir, 'Annotations')
    with open(os.path.join(voc_dir, 'ImageSets', 'Main', dataset_type + '.txt')) as f:
        file_list = f.read().splitlines()
    annotations, images_paths, difficult_list = extract_voc_data(annotations_dir, images_dir, file_list, class_list)
    dataset = ObjectDetectionDataset(annotations, images_paths, class_list, transform, difficult_list)
    return dataset
  
class Rescale(object):
    """Rescale the image in a sample to a given size.

    Args:
        output_size (tuple or int): Desired output size. If tuple, output is
            matched to output_size. If int, smaller of image edges is matched
            to output_size keeping aspect ratio the same.
        max_output_size (int): Maximum output size of the larger of image edges.
    """

    def __init__(self, output_size, max_output_size = 0):
        assert isinstance(output_size, (int, tuple))
        assert isinstance(max_output_size, int)
        self.output_size = output_size
        self.max_output_size = max_output_size

    def __call__(self, sample):
        image, annotations, scale = sample['image'], sample['annotations'], sample['scale']

        h, w = image.shape[:2]
        if isinstance(self.output_size, int):
            if h > w:
                if (self.max_output_size and self.max_output_size * w >= self.output_size * h):
                    new_h, new_w = self.output_size * h / w, self.output_size
                else:
                    new_h, new_w = self.max_output_size, self.max_output_size * w / h
            else:
                if (self.max_output_size and self.max_output_size * h >= self.output_size * w):
                    new_h, new_w = self.output_size, self.output_size * w / h
                else:
                    new_h, new_w = self.max_output_size * h / w, self.max_output_size
        else:
            new_h, new_w = self.output_size

        new_h, new_w = int(new_h), int(new_w)

        img = transform.resize(image, (new_h, new_w), mode = None, anti_aliasing = None)

        # h and w are swapped for annotations because, for images,
        # x and y axes are axis 1 and 0 respectively
        if (len(annotations) > 0):
            annotations = annotations * [1, new_w / w, new_h / h, new_w / w, new_h / h]
        scale = (scale[0] * new_w / w, scale[1] * new_h / h)

        return {'image': img, 'annotations': annotations, 'scale': scale}

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        image, annotations, scale = sample['image'], sample['annotations'], sample['scale']

        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        image = image.transpose((2, 0, 1))
        return {'image': torch.from_numpy(image).float(), 'annotations': torch.from_numpy(annotations).float(), 'scale': scale}

class Normalize(object):
    """Normalize the image Tensor's channels."""

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, sample):
        image, annotations, scale = sample['image'], sample['annotations'], sample['scale']
        image = F.normalize(image, self.mean, self.std)
        return {'image': image, 'annotations': annotations, 'scale': scale}

class FlipHorizontal(object):
    """Flip the image and its annotations horizontally."""

    def __call__(self, sample):
        image, annotations, scale = sample['image'], sample['annotations'], sample['scale']
        if (numpy.random.rand() > 0.5):
            image = numpy.flip(image, axis = 1).copy()
            if (len(annotations > 0)):
                annotations[:, 1] = image.shape[1] - 1 - annotations[:, 1] - annotations[:, 3]
        return {'image': image, 'annotations': annotations, 'scale': scale}
