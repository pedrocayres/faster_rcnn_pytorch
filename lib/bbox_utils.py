import torch

# Transform bbox using the coeficients in bbox_tfm
def transform_bbox(bbox, bbox_tfm):
    bbox[:, 0] = bbox[:, 0] + bbox[:, 2] * (bbox_tfm[:, 0] + 0.5)
    bbox[:, 1] = bbox[:, 1] + bbox[:, 3] * (bbox_tfm[:, 1] + 0.5)
    bbox[:, 2] *= torch.exp(bbox_tfm[:, 2])
    bbox[:, 3] *= torch.exp(bbox_tfm[:, 3])
    bbox[:, 0] -= 0.5 * bbox[:, 2]
    bbox[:, 1] -= 0.5 * bbox[:, 3]
    return bbox

# Compute the bounding-box transformation coeficients that transforms roi_bbox into gt_bbox
def compute_bbox_tfm_target(roi_bbox, gt_bbox):
    bbox_tfm = torch.zeros(roi_bbox.size(), device = roi_bbox.device)
    bbox_tfm[:, 0] = (gt_bbox[:, 0] + 0.5 * gt_bbox[:, 2] - roi_bbox[:, 0] - 0.5 * roi_bbox[:, 2]) / roi_bbox[:, 2]
    bbox_tfm[:, 1] = (gt_bbox[:, 1] + 0.5 * gt_bbox[:, 3] - roi_bbox[:, 1] - 0.5 * roi_bbox[:, 3]) / roi_bbox[:, 3]
    bbox_tfm[:, 2] = torch.log(gt_bbox[:, 2] / roi_bbox[:, 2])
    bbox_tfm[:, 3] = torch.log(gt_bbox[:, 3] / roi_bbox[:, 3])
    return bbox_tfm

# Clip the bounding-boxes into the image
def clip_bbox(bbox, width, height):
    delta = bbox[:, 0].clamp(0, width - 1) - bbox[:, 0]
    bbox[:, 0] += delta
    bbox[:, 2] = torch.clamp(bbox[:, 0] + bbox[:, 2] - delta, 0, width - 1) - bbox[:, 0]
    delta = bbox[:, 1].clamp(0, height - 1) - bbox[:, 1]
    bbox[:, 1] += delta
    bbox[:, 3] = torch.clamp(bbox[:, 1] + bbox[:, 3] - delta, 0, height - 1) - bbox[:, 1]
    return bbox
